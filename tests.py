from datetime import datetime, timedelta
import unittest
from app import app, db
from app.models import User, Post, Project, Protocol, Item

#Exemple from the tutorial
#Test for followers
class UserModelCase(unittest.TestCase):
    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_password_hashing(self):
        u = User(username='susan')
        u.set_password('cat')
        self.assertFalse(u.check_password('dog'))
        self.assertTrue(u.check_password('cat'))

    def test_avatar(self):
        u = User(username='john', email='john@example.com')
        self.assertEqual(u.avatar(128), ('https://www.gravatar.com/avatar/'
                                         'd4c74594d841139328695756648b6bd6'
                                         '?d=identicon&s=128'))

    def test_follow(self):
        u1 = User(username='john', email='john@example.com')
        u2 = User(username='susan', email='susan@example.com')
        db.session.add(u1)
        db.session.add(u2)
        db.session.commit()
        self.assertEqual(u1.followed.all(), [])
        self.assertEqual(u1.followers.all(), [])

        u1.follow(u2)
        db.session.commit()
        self.assertTrue(u1.is_following(u2))
        self.assertEqual(u1.followed.count(), 1)
        self.assertEqual(u1.followed.first().username, 'susan')
        self.assertEqual(u2.followers.count(), 1)
        self.assertEqual(u2.followers.first().username, 'john')

        u1.unfollow(u2)
        db.session.commit()
        self.assertFalse(u1.is_following(u2))
        self.assertEqual(u1.followed.count(), 0)
        self.assertEqual(u2.followers.count(), 0)

    def test_follow_posts(self):
        # create four users
        u1 = User(username='john', email='john@example.com')
        u2 = User(username='susan', email='susan@example.com')
        u3 = User(username='mary', email='mary@example.com')
        u4 = User(username='david', email='david@example.com')
        db.session.add_all([u1, u2, u3, u4])

        # create four posts
        now = datetime.utcnow()
        p1 = Post(body="post from john", author=u1,
                  timestamp=now + timedelta(seconds=1))
        p2 = Post(body="post from susan", author=u2,
                  timestamp=now + timedelta(seconds=4))
        p3 = Post(body="post from mary", author=u3,
                  timestamp=now + timedelta(seconds=3))
        p4 = Post(body="post from david", author=u4,
                  timestamp=now + timedelta(seconds=2))
        db.session.add_all([p1, p2, p3, p4])
        db.session.commit()

        # setup the followers
        u1.follow(u2)  # john follows susan
        u1.follow(u4)  # john follows david
        u2.follow(u3)  # susan follows mary
        u3.follow(u4)  # mary follows david
        db.session.commit()

        # check the followed posts of each user
        f1 = u1.followed_posts().all()
        f2 = u2.followed_posts().all()
        f3 = u3.followed_posts().all()
        f4 = u4.followed_posts().all()
        self.assertEqual(f1, [p2, p4, p1])
        self.assertEqual(f2, [p2, p3])
        self.assertEqual(f3, [p3, p4])
        self.assertEqual(f4, [p4])

#Test for ProjectProtocols
class ProjectModelCase(unittest.TestCase):
    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_include_protocol(self):
        p1 = Project(name="Barrett_fly", noSOW="CGEN0320")
        pr1 = Protocol(name="Extraction")
        db.session.add(p1)
        db.session.add(pr1)
        db.session.commit()
        #No protocols is assign to neither of the project
        self.assertEqual(p1.protocols.all(), [])

        p1.include(pr1)
        db.session.commit()
        #check if protocol1 is in project 1
        self.assertTrue(p1.exist(pr1))
        self.assertEqual(p1.protocols.count(), 1)
        self.assertEqual(p1.protocols.first().name, "Extraction")

        p1.exclude(pr1)
        db.session.commit()
        self.assertFalse(p1.exist(pr1))
        self.assertEqual(p1.protocols.count(), 0)

    def test_show_protocols(self):
        # create two project
        p1 = Project(name="Barrett_fly", noSOW="CGEN0320")
        p2 = Project(name="Leclair_blood", noSOW="CGEN0310")
        db.session.add_all([p1,p2])

        # create 4 protocols
        pr1 = Protocol(name="Extraction", id="001")
        pr2 = Protocol(name="Clean-Up", id="002")
        pr3 = Protocol(name="Library1", id="003")
        pr4 = Protocol(name="Library2", id="004")
        db.session.add_all([pr1, pr2, pr3, pr4])
        db.session.commit()

        # setup the followers
        p1.include(pr1)  # john follows susan
        p1.include(pr2)  # john follows david
        p1.include(pr4)  # susan follows mary
        p2.include(pr3)  # mary follows david
        db.session.commit()

        # check the followed posts of each user
        f1 = p1.included_protocols().all()
        f2 = p2.included_protocols().all()
        self.assertEqual(f1, [pr1, pr2, pr4])
        self.assertEqual(f2, [pr3])

#Tests pour itemsProtocols
class ProtocolModelCase(unittest.TestCase):
    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_include_item(self):
        pr1 = Protocol(name="Extraction", id="001")
        i1 = Item(item="Tip10", id=1)
        i2 = Item(item="SPRI_beads", id=4)
        db.session.add(i1, i2)
        db.session.add(pr1)
        db.session.commit()
        #No protocols is assign to neither of the protocols
        self.assertEqual(pr1.items, [])

        pr1.include(i1, 5, 1)
        db.session.commit()
        #check if protocol1 is in project 1
        self.assertTrue(pr1.item_exist(i1))
        self.assertEqual(pr1.items_count(), 1)
        self.assertEqual(pr1.items_first().item_id, 1)

        pr1.include(i2, 60, 2)
        pr1.exclude(i1)
        db.session.commit()
        self.assertFalse(pr1.item_exist(i1))
        self.assertEqual(pr1.items_count(), 1)

    def test_show_items(self):
        # create 4 items
        i1 = Item(item="tip10", id=1)
        i2 = Item(item="SPRI_beads", id=4)
        i3 = Item(item="AP96_200", id=10)
        i4 = Item(item="96w_skirted", id=15)
        db.session.add_all([i1,i2,i3,i4])

        # create 4 protocols
        pr1 = Protocol(name="Extraction", id="001")
        pr2 = Protocol(name="Clean-Up", id="002")
        db.session.add_all([pr1, pr2])
        db.session.commit()

        # setup the items
        pr1.include(i1, 5, 1)
        pr1.include(i3, 45, 1)
        # print("check duplicate")
        pr1.include(i3, 45, 1)
        # print("Check item in two protocols")
        pr1.include(i2, 60, 2)
        pr2.include(i2, 60, 2)
        pr2.include(i4, 1, 2)
        # print(pr1.included_items().all())
        # print(pr2.included_items().all())
        db.session.commit()

        # check the items of each protcols
        f1 = pr1.included_items().all()
        f2 = pr2.included_items().all()
        self.assertEqual(f1, [i1, i2, i3])
        self.assertEqual(f2, [i2, i4])

#Execution of the tests
if __name__ == '__main__':
    unittest.main(verbosity=2)
