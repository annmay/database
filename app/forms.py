from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, TextAreaField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length
from app.models import User, Item, Company, Category


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

class EditProfileForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    about_me = TextAreaField('About me', validators=[Length(min=0, max=140)])
    submit = SubmitField('Submit')

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError('Please use a different username.')

class NewItemForm(FlaskForm):
    noCatalogue = StringField('Catalog Number', validators=[DataRequired()])
    item = StringField('Item Name', validators=[DataRequired()])
    category = StringField('Item Category', validators=[DataRequired()]) ## Drop down menu with choices? aim = uniformise equivalent item
    format = IntegerField('Item Format', validators=[DataRequired()])
    unit = StringField('Format unit', validators=[DataRequired()])
    cie = StringField('Company Name', validators=[DataRequired()])
    submit = SubmitField('Add New Item')

    def validate_noCatalogue(self, noCatalogue):
        item = Item.query.filter_by(noCatalogue=noCatalogue.data).first()
        if item is not None:
            raise ValidationError('The item already exist in the system')

    def validate_catagory(self, category):
        item = Item.query.filter_by(category_id=category.data).first()
        if item is None:
            raise ValidationError('Please choose an existing category or create a new one (administrator only)')

    # def validate_company(self, cie):
    #     item = Item.query.filter_by(cie_id=cie_id.data).first()
    #     if item is None:
    #         raise ValidationError('Please choose an existing Company or create a new one.')

class NewCieForm(FlaskForm):
    cieName = StringField('Company Name', validators=[DataRequired()])
    address = StringField('Company address', validators=[DataRequired()]) ## Form specific for address?
    meanDeliveryTime = IntegerField('Mean Delivery time (days)', validators=[DataRequired()])
    submit = SubmitField('Add New Company')

    def validate_cieName(self, cieName):
        cieName = Company.query.filter_by(cieName=cieName.data).first()
        if item is not None:
            raise ValidationError('The company already exist in the system')

class NewCategoryForm(FlaskForm):
    category = StringField('Item category', validators=[DataRequired()])
    submit = SubmitField('Add New Item Category')

    def validate_category(self, category):
        category = Category.query.filter_by(category=category.data).first()
        if category is not None:
            raise ValidationError('This item category already exist in the system')
