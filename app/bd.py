import sqlite3

###DATABASE###

def init_db():
    conn = sqlite3.connect('inventory.db')
    conn.execute("PRAGMA foreign_keys = 1")
    query = ''' CREATE TABLE IF NOT EXISTS Items (
                    noCatalogue     TEXT    NOT NULL,
                    idCie           INTEGER NOT NULL,
                    nomItem         TEXT    NOT NULL,
                    description     TEXT    NOT NULL,
                    format          INTEGER NOT NULL,
                    unit            TEXT    NOT NULL,
                    PRIMARY KEY (noCatalogue),
                    FOREIGN KEY (idCie) REFERENCES Companies(idCie)
            ) '''
    conn.cursor().execute(query)

    query = ''' CREATE TABLE IF NOT EXISTS Companies (
                    idCie               INTEGER NOT NULL,
                    nomCie              TEXT    NOT NULL,
                    address             TEXT    NOT NULL,
                    meanDeliveryTime    INTEGER,
                    PRIMARY KEY (idCie)
            ) '''
    conn.cursor().execute(query)

    query = ''' CREATE TABLE IF NOT EXISTS SaleAgents (
                    idAgent     TEXT    NOT NULL,
                    idCie       INTEGER NOT NULL,
                    nom         TEXT    NOT NULL,
                    prenom      TEXT    NOT NULL,
                    noTel       INTEGER NOT NULL,
                    email       TEXT    NOT NULL,
                    PRIMARY KEY (idAgent),
                    FOREIGN KEY (idCie) REFERENCES Companies(idCie)
            ) '''
    conn.cursor().execute(query)

    query = ''' CREATE TABLE IF NOT EXISTS QtyInventory (
                    noCatalogue     TEXT        NOT NULL,
                    idLot           INTEGER     NOT NULL,
                    qty             INTEGER     NOT NULL,
                    idLocation      INTEGER     NOT NULL,
                    expirationDate  DATETIME    NOT NULL,
                    PRIMARY KEY (idLot),
                    FOREIGN KEY (noCatalogue) REFERENCES Items(noCatalogue),
                    FOREIGN KEY (idLocation) REFERENCES Locations(idLocation)
            ) '''
    conn.cursor().execute(query)

    query = ''' CREATE TABLE IF NOT EXISTS Locations (
                    idLocation  INTEGER     NOT NULL,
                    name        INTEGER     NOT NULL,
                    floor       INTEGER     NOT NULL,
                    room        TEXT,
                    temperature INTEGER     NOT NULL,
                    comments    TEXT,
                    PRIMARY KEY (idLocation)
            ) '''
    conn.cursor().execute(query)

    query = ''' CREATE TABLE IF NOT EXISTS QtyOrdered (
                    noCatalogue     TEXT        NOT NULL,
                    id              INTEGER     NOT NULL,
                    POnumber        TEXT        NOT NULL,
                    qty             INTEGER     NOT NULL,
                    Status          TEXT,
                    PRIMARY KEY (id),
                    FOREIGN KEY (noCatalogue) REFERENCES Items(noCatalogue)
            ) ''' ##Status: NULL or Received (Hiden to the user, used to generate views)
    conn.cursor().execute(query)

    query = '''CREATE TABLE IF NOT EXISTS Kanbans (
                    noCatalogue     TEXT        NOT NULL,
                    id              INTEGER     NOT NULL,
                    min             INTEGER     NOT NULL,
                    max             INTEGER     NOT NULL,
                    PRIMARY KEY (id),
                    FOREIGN KEY (noCatalogue) REFERENCES Items(noCatalogue)
            ) '''
    conn.cursor().execute(query)

    query = ''' CREATE TABLE IF NOT EXISTS Projects (
                    noSOW       TEXT    NOT NULL,
                    Category    TEXT    NOT NULL,
                    Status      TEXT    NOT NULL,
                    ClientName  TEXT    NOT NULL,
                    NbSample    INTEGER NOT NULL,
                    PRIMARY KEY (noSOW)
            ) '''
    conn.cursor().execute(query)

    query = '''CREATE TABLE IF NOT EXISTS Protocols (
                    idProtocol      INTEGER NOT NULL,
                    ProtocolName    TEXT    NOT NULL'
                    lien            TEXT    NOT NULL,
                    activeTime      INTEGER NOT NULL,
                    elapsedTime     INTEGER NOT NULL,
                    comments        TEXT,
                    PRIMARY KEY (idProtocol)
            ) '''
    conn.cursor().execute(query)

    query = '''CREATE TABLE IF NOT EXISTS MaterialProtocols (
                    noCatalogue     TEXT        NOT NULL,
                    idProtocol      INTEGER     NOT NULL,
                    qty             INTEGER     NOT NULL,
                    nbSample        INTEGER     NOT NULL,
                    FOREIGN KEY (noCatalogue) REFERENCES Items(noCatalogue),
                    FOREIGN KEY (idProtocol) REFERENCES Protocols(idProtocol)
            ) '''
    conn.cursor().execute(query)

    query = '''CREATE TABLE IF NOT EXISTS ProjectsProtocols (
                    noSOW           TEXT        NOT NULL,
                    idProtocol      INTEGER     NOT NULL,
                    FOREIGN KEY (noSOW) REFERENCES Projects(noSOW),
                    FOREIGN KEY (idProtocol) REFERENCES Protocols(idProtocol)
            ) '''
    conn.cursor().execute(query)

init_db()
# print("db done")


###FUNCTIONS###

##Functions for the Items Table
def addItems(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("INSERT INTO Items (noCatalogue,idCie,nomItem,description,format,unit) VALUES (?,?,?,?,?,?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def removeItems(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("DELETE FROM Items WHERE noCatalogue=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateItemsFormat(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Items set format=(?) where noCatalogue=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)



##Functions for the Companies Table
def addCie(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("INSERT INTO Companies (idCie,nomCie,address,meanDeliveryTime) VALUES (?,?,?,?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def removeCie(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("DELETE FROM Companies WHERE idCie=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateCieAddress(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Companies set address=(?) where idCie=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateCieDeliveryTime(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Companies set meanDeliveryTime=(?) where idCie=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)



##Functions for the QtyInventory Table
def addInventory(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("INSERT INTO QtyInventory (noCatalogue,idLot,qty,idLocation,expirationDate) VALUES (?,?,?,?,?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def removeInventory(record): #Ne pas utiliser, faire une archive des ligne = à 0
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("DELETE FROM QtyInventory WHERE idLot=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateQtyInventory(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update QtyInventory set qty=(?) where idLot=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateInventoryLocation(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update QtyInventory set idLocation=(?) where idLot=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)



##Functions for the Locations Table
def addLocations(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("INSERT INTO Locations (idLocation,name,floor,room,temperature,comments) VALUES (?,?,?,?,?,?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def removeLocations(record): #Ne pas utiliser, faire une archive des ligne = à 0
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("DELETE FROM Locations WHERE idLocation=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateLocationsName(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Locations set name=(?) where idLocation=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateLocations(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Locations set floor=(?), room=(?) where idLocation=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateLocationsComments(record): ## Fonction à retravailler; c'est trop restrictif en ce moment.
    try:
        conn = sqlite3.connect('inventory.db')
        old = conn.cursor().execute("SELECT comments FROM Locations WHERE idLocation=(?)", record[1])
        print(old)
        record = (str(old) + " " + record[0], record[1])
        conn.cursor().execute("Update Locations set comments=(?) where idLocation=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)



##Functions for the QtyInventory Table
def addQtyOrdered(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("INSERT INTO QtyOrdered (noCatalogue,id,POnumber,qty) VALUES (?,?,?,?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def removeQtyOrdered(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("DELETE FROM QtyOrdered WHERE id=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateQtyOrdered(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update QtyOrdered set qty=(?) where id=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)



##Functions for the Kanbans Table
def addKanbans(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("INSERT INTO Kanbans (noCatalogue,id,min,max) VALUES (?,?,?,?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def removeKanbans(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("DELETE FROM Kanbans WHERE id=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateKanbans(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Kanbans set min=(?), max=(?) where id=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)



##Functions for the Projects Table
def addProjects(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("INSERT INTO Projects (noSOW,Category,Status,ClientName,NbSample) VALUES (?,?,?,?,?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def removeProjects(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("DELETE FROM Projects WHERE noSOW=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateProjectsSatus(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Projects set Status=(?) where noSOW=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateProjectsInfo(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Projects set ClientName=(?), NbSample=(?) where noSOW=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)


##Functions for the Protocols Table
def addProtocols(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("INSERT INTO Protocols (idProtocol,ProtocolName,lien,activeTime,elapsedTime,comments) VALUES (?,?,?,?,?,?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def removeProtocols(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("DELETE FROM Protocols WHERE idProtocol=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateProtocolsLink(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Protocols set lien=(?) where idProtocol=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateProtocolsTimes(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Protocols set activeTime=(?), elapsedTime=(?) where idProtocol=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateProtocolsComments(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update Protocols set comments=(?) where idProtocol=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)



##Functions for the MaterialProtocols Table
def addMaterialProtocols(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("INSERT INTO MaterialProtocols (noCatalogue,idProtocol,qty,nbSample) VALUES (?,?,?,?,?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def removeMaterialProtocols(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("DELETE FROM MaterialProtocols WHERE noCatalogue=(?) AND idProtocol=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def updateMaterialProtocols(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("Update MaterialProtocols set qty=(?), nbSampl(?) where noCatalogue=(?) AND idProtocol=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)



##Functions for the ProjectsProtocols Table
def addProjectsProtocols(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("INSERT INTO ProjectsProtocols (noSOW,idProtocol) VALUES (?,?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def removeProjectsProtocols(record):
    try:
        conn = sqlite3.connect('inventory.db')
        conn.cursor().execute("DELETE FROM ProjectsProtocols WHERE noSOW=(?) AND idProtocol=(?)", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)




###VIEWS###

def getRecord(record): #record = (TableName, ColName, Value)
    value = []
    try:
        conn = sqlite3.connect('inventory.db')
        value = conn.cursor().execute("select * from (?) where (?)=(?)", record)
    except sqlite3.Error as e:
        print (e)
    return value

def getProjects():
    records = []
    try:
        conn = sqlite3.connect('inventory.db')
        records = conn.cursor().execute("select * from Projects")
    except sqlite3.Error as e:
        print (e)
    return records

def getInventory():
    records = []
    try:
        conn = sqlite3.connect('inventory.db')
        records = conn.cursor().execute("""select noCatalogue, nomItem, QtyInventory.qty as QtyInv, QtyOrdered.qty as QtyOrd
                                        from Items
                                        left outer join QtyInventory on noCatalogue
                                        left outer join QtyOrdered on noCatalogue """)
    except sqlite3.Error as e:
        print (e)
    return records
