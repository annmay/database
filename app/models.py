from datetime import datetime
from app import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from hashlib import md5
from sqlalchemy.ext.associationproxy import association_proxy

followers = db.Table('followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
)

projectProtocols = db.Table('projectProtocols',
    db.Column('project_id', db.Integer, db.ForeignKey('project.id'), primary_key=True),
    db.Column('protocol_id', db.Integer, db.ForeignKey('protocol.id'), primary_key=True)
)


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    about_me = db.Column(db.String(140))
    posts = db.relationship('Post', backref='author', lazy='dynamic') #One to many relationship, specify on the ONE side
    followed = db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'), lazy='dynamic')

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(digest, size)

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        return self.followed.filter(
            followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):
        followed = Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
                followers.c.follower_id == self.id)
        own = Post.query.filter_by(user_id=self.id)
        return followed.union(own).order_by(Post.timestamp.desc())

    def __repr__(self):
        return '<User {}>'.format(self.username)

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Post {}>'.format(self.body)

class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    noCatalogue = db.Column(db.String(140), index=True, unique=True)
    item = db.Column(db.String(140))
    format = db.Column(db.Integer)
    unit = db.Column(db.String(50))
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    cie_id = db.Column(db.Integer, db.ForeignKey('company.id'))
    inventory = db.relationship('QtyInventory', backref='QtyInv', lazy='dynamic') #One to many relationship, specify on the ONE side
    order = db.relationship('QtyOrdered', backref='QtyOrd', lazy='dynamic') #One to many relationship, specify on the ONE side
    kanban = db.relationship('Kanban', backref='kanban', lazy='dynamic')
    protocols = association_proxy('item_protocols', 'protocol', creator=lambda p: ItemsProtocols(protocol=p))

    def __repr__(self):
        return 'Items(%s)' % repr(self.item)

    def exist(self, item):
        return db.session.query(self).filter(self.id==item.id).count() > 0

class Category(db.Model): #Category of Items (e.g. tips filtrée 10µl stérile)
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(300), index=True, unique=True) #description of the item category
    item_id = db.relationship('Item', backref='Items', lazy='dynamic') #One to many relationship, specify on the ONE side

class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cieName = db.Column(db.String(140), index=True, unique=True)
    address = db.Column(db.String(400))
    meanDeliveryTime = db.Column(db.Integer)
    items = db.relationship('Item', backref='product', lazy='dynamic') #One to many relationship, specify on the ONE side
    agents = db.relationship('SaleAgents', backref='agent', lazy='dynamic') #One to many relationship, specify on the ONE side

    def __repr__(self):
        return '<Company {}>'.format(self.cieName)

class SaleAgents(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    noCatalogue = db.Column(db.String(140), index=True, unique=True)
    agentName = db.Column(db.String(140))
    email = db.Column(db.String(120), index=True, unique=True)
    noTel = db.Column(db.Integer)
    cie_id = db.Column(db.Integer, db.ForeignKey('company.id'))

    def __repr__(self):
        return '<Agent {}>'.format(self.agentName)

class QtyInventory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    noLot = db.Column(db.Integer, index=True, unique=True)
    qty = db.Column(db.Integer)
    expirationDate = db.Column(db.DateTime)
    loc_id = db.Column(db.Integer, db.ForeignKey('location.id'))
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'))
    Order_PO = db.Column(db.String(15), db.ForeignKey('qty_ordered.POnumber'))

    def __repr__(self):
        return '<Inventory {}>'.format(self.qty)

class Location(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    locName = db.Column(db.String(150))
    floor = db.Column(db.Integer)
    room = db.Column(db.Integer)
    temperature = db.Column(db.String(20))
    inventory = db.relationship('QtyInventory', backref='Qty', lazy='dynamic') #One to many relationship, specify on the ONE side

    def __repr__(self):
        return '<Location {}>'.format(self.LocName)

class QtyOrdered(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    POnumber = db.Column(db.String(15), index=True)
    qty = db.Column(db.Integer)
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'))
    status = db.Column(db.String(15))##Status: NULL or Received (Hiden to the user, used to generate views)
    inventory = db.relationship('QtyInventory', backref='qty_received', lazy='dynamic')

    def __repr__(self):
        return '<Ordered {}>'.format(self.qty)

class Kanban(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    min = db.Column(db.Integer)
    max = db.Column(db.Integer)
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'))

    def __repr__(self):
        return '<Kanban {}>'.format(self.id)

class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    noSOW = db.Column(db.String(20), index=True, unique=True)
    name = db.Column(db.String(200), index=True, unique=True)
    category = db.Column(db.String(60))
    status = db.Column(db.String(60))
    clientName = db.Column(db.String(200))
    nbSample = db.Column(db.Integer)
    protocols = db.relationship(
        'Protocol', secondary=projectProtocols,
        # primaryjoin=(projectProtocols.c.project_id == self.id),
        # secondaryjoin=(projectProtocols.c.protocol_id == protocol.id),
        backref=db.backref('projectProtocols', lazy='dynamic'), lazy='dynamic')

    def include(self, protocol):
        if not self.exist(protocol):
            self.protocols.append(protocol)

    def exclude(self, protocol):
        if self.exist(protocol):
            self.protocols.remove(protocol)

    def exist(self, protocol):
        return self.protocols.filter(
            projectProtocols.c.protocol_id == protocol.id).count() > 0

    def included_protocols(self):
        return Protocol.query.join(
            projectProtocols, (projectProtocols.c.protocol_id == Protocol.id)).filter(
                projectProtocols.c.project_id == self.id).order_by(
                    Protocol.id.asc())



    def __repr__(self):
        return '<Project {}>'.format(self.name)

class Protocol(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), index=True, unique=True)
    lien = db.Column(db.String(300))
    activeTime = db.Column(db.Integer)
    elapsedTime = db.Column(db.Integer)
    comment = db.Column(db.String(600))
    # items = db.relationship("ItemsProtocols", back_populates="protocol")
    items = association_proxy('protocol_items', 'item', creator=lambda i: ItemsProtocols(item=i))

    def include(self, item, qty, sample):
        if not self.item_exist(item):
            a = ItemsProtocols(item, self, qty, sample)

    def exclude(self, item):
        if self.item_exist(item):
            # print(self.items, "start")
            db.session.query(ItemsProtocols).filter(ItemsProtocols.protocol_id==self.id).filter(
                ItemsProtocols.item_id==item.id).delete()
            # print(self.items, "finish")


    def items_first(self):
        return db.session.query(ItemsProtocols.item_id).filter(ItemsProtocols.protocol_id==self.id).first()

    def items_count(self):
        return db.session.query(ItemsProtocols.item_id).filter(ItemsProtocols.protocol_id==self.id).count()

    def item_exist(self, item):
        return db.session.query(ItemsProtocols.item_id).filter(ItemsProtocols.protocol_id==self.id).filter(
            ItemsProtocols.item_id==item.id).count() > 0

    def included_items(self):
        return Item.query.join(
            ItemsProtocols, (ItemsProtocols.__table__.c.item_id == Item.id)).filter(
                ItemsProtocols.__table__.c.protocol_id == self.id).order_by(
                    Item.id.asc())


    def __repr__(self):
        return '<Protocol {}>'.format(self.name)

class ItemsProtocols(db.Model):
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'), primary_key=True)
    protocol_id = db.Column(db.Integer, db.ForeignKey('protocol.id'), primary_key=True)
    qty = db.Column(db.Integer)
    nbSample = db.Column(db.Integer)
    protocol = db.relationship(Protocol,
                backref=db.backref("protocol_items",
                                cascade="all, delete-orphan",
                                lazy='dynamic')
            )
    item = db.relationship("Item", backref='item_protocols')
    # item = db.relationship("Item", back_populates="protocols")
    # protocol = db.relationship("Protocol", back_populates="items")
    def __init__(self, item=None, protocol=None, qty=None, nbSample=None):
        self.item = item
        self.protocol = protocol
        self.qty = qty
        self.nbSample = nbSample


@login.user_loader
def load_user(id):
    return User.query.get(int(id))
