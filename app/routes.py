from flask import render_template, flash, redirect, url_for, request
from werkzeug.urls import url_parse
from flask_login import current_user, logout_user, login_required, login_user
from app import app, db
from app.forms import LoginForm, RegistrationForm, NewCategoryForm, NewItemForm, NewCieForm, EditProfileForm
from app.models import User, Company, Category
from datetime import datetime


@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@app.route('/')
@app.route('/index')
@login_required
def index():
    posts = [
        {
            'author': {'username': 'John'},
            'body': 'noSOW added as a New Project on DATE'
        },
        {
            'author': {'username': 'Susan'},
            'body': 'ProtocolName was ran on DATE'
        }
    ]
    return render_template('index.html', title='Home', posts=posts)

@app.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    posts = [
        {'author': user, 'body': 'Test post #1'},
        {'author': user, 'body': 'Test post #2'}
    ]
    return render_template('user.html', user=user, posts=posts)

@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)

@app.route('/newItem', methods=['GET', 'POST'])
@login_required
def newItem():
    form = NewItemForm()
    if form.validate_on_submit():
        item = Item(noCatalogue=form.noCatalogue.data, itemName=form.itemName.data, \
         format=form.format.data, unit=form.unit.data)
        db.session.add(item)
        db.session.commit()
        flash('Congratulations, item was added successully!')
        return redirect(url_for('index'))
    return render_template('AddItem.html', title='New Item', form=form)

@app.route('/newCompany', methods=['GET', 'POST'])
@login_required
def newCie():
    form = NewCieForm()
    if form.validate_on_submit():
        cie = Company(cieName=form.cieName.data, address=form.address.data, meanDeliveryTime=form.meanDeliveryTime.data)
        db.session.add(cie)
        db.session.commit()
        flash('Congratulations, company was added successully!')
        return redirect(url_for('index'))
    return render_template('AddCie.html', title='New Company', form=form)

@app.route('/newItemCategory', methods=['GET', 'POST'])
@login_required
def newCategory():
    form = NewCategoryForm()
    if form.validate_on_submit():
        category = Category(category=form.category.data)
        db.session.add(category)
        db.session.commit()
        flash('Congratulations, Item category was added successully!')
        return redirect(url_for('index'))
    return render_template('AddCategory.html', title='New Item Category', form=form)

@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title='Edit Profile',
                           form=form)
