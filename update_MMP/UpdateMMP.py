"""
Program: Order Update from MMP
Author: Anne-Marie Roy
Date: Summer 2021

This program takes 3 files in input:

Requisition.csv (col: PR ID, PR Name, PR Date, Create Date, Complete Date, Requisitioner, PR Line #, Vendor Name, Vendor ID, Fund, Activity, Product Description, SKU/Catalog #, Unit Price,
Quantity, Extended Price, Currency, Line Status, Requisition Total)

PO.csv (col: PO ID, PO #, Status, PR ID, Requisitioner, Creation Date, Vendor Name, Fund, Activity, PO Line #, PO Line ID, Product Description, SKU/Catalog #, Quantity, Unit Price, Extended Price,
Receive Status, Qty Net Received, Invoice Status, Qty Net Invoiced, Settlement Status	)

CurrentOrders.csv (col: Date Added, Requisition#, PO#, Line#, Creation Date, Submission Date, PO date, Requisitioner, Fund, Activity, Vendor Name, Vendor ID, Product Description, Catalog#, Qty,
Unit Price, Extended Price, Currency, Received Qty, Invoiced Qty, Receive Status, Invoice Status, Settlement Status)

The principal output is an Excel file (each of the sheets are also generated as separate csv files)
"""

import csv, sys, openpyxl
import pandas as pd
import numpy as np
from datetime import date, datetime
from glob import glob

###1 Load the current orders
# data = pd.read_csv(sys.argv[3])
# col_orders = data.columns
# print(len(data.index), "pré-réinitialisation")
# #Change some data for NaN (to allow for updating them)
# data.loc[(~data["Receive Status"].str.contains("Fully", na=False), "Receive Status")] = np.nan
# data.loc[(~data["Invoice Status"].str.contains("Fully", na=False), "Invoice Status")] = np.nan
# data.loc[(~data["Receive Status"].str.contains("Fully", na=False), "Receive Qty")] = np.nan
# data.loc[(~data["Invoice Status"].str.contains("Fully", na=False), "Invoice Qty")] = np.nan
# data.loc[(~data["Settlement Status"].str.contains("Closed", na=False), "Settlement Status")] = np.nan
# data=data.astype(str)
#
# ###2 read the req and add whatever is missing to the current orders
# range1 = [i for i in range(0,19)]
# dfReq = pd.read_csv(sys.argv[1], usecols = range1)
#
# ##changing ColName to fit the CurrentOrders ColNames
# ReqNames = {"PR ID":"Requisition#", "PR Name":"PR Name", "PR Date":"Submission Date", "Create Date":"Creation Date",
# "Complete Date":"Complete Date", "Requisitioner":"Requisitioner", "PR Line #":"Line#", "Vendor Name":"Vendor Name",
# "Vendor ID":"Vendor ID", "Fund":"Fund", "Activity":"Activity", "Product Description":"Product Description",
# "SKU/Catalog #":"Catalog#", "Unit Price":"Unit Price", "Quantity":"Qty", "Extended Price":"Extended Price",
# "Currency":"Currency", "Line Status":"Line Status", "Requisition Total":"Requisition Total"}
# dfReq2 = dfReq.rename(columns=ReqNames)
# colReq = dfReq2.columns.intersection(data.columns)
# dfReq2 = dfReq2[colReq] #Reordering the columns
# dfReq2 = dfReq2.astype(str)
# print("Req: ", len(dfReq2.index))
#
# ##Puting the 2 df together
# update = data.merge(dfReq2, how='outer').drop_duplicates(subset=["Requisition#", "Catalog#"], keep="first")
# update = update.groupby(["Requisition#", "Catalog#"]).first().reset_index()
# test = update.copy() #create a copies to validate data later
# update = update[col_orders] #Reordering the columns
# print("Update Req: ", len(update.index))
#
# ###3 read the PO and update every line in the current orders (if different)
# range1 = [i for i in range(0,21)]
# dfPO = pd.read_csv(sys.argv[2], usecols = range1)
#
# ##changing ColName to fit the CurrentOrders
# PONames = {"PO ID":"PO ID", "PO #":"PO#", "Status":"Status", "PR ID":"Requisition#", "Requisitioner":"Requisitioner",
# "Creation Date":"PO date", "Vendor Name":"Vendor Name", "Fund":"Fund", "Activity":"Activity", "PO Line #":"Line#",
# "PO Line ID":"PO Line ID", "Product Description":"Product Description", "SKU/Catalog #":"Catalog#", "Quantity":"Qty",
# "Unit Price":"Unit Price", "Extended Price":"Extended Price", "Receive Status":"Receive Status", "Qty Net Received":"Received Qty",
# "Invoice Status":"Invoice Status", "Qty Net Invoiced":"Invoiced Qty", "Settlement Status":"Settlement Status"}
# dfPO2 = dfPO.rename(columns=PONames)
# dfPO2 = dfPO2.astype(str)
# print("PO :", len(dfPO2.index))
#
# ##Puting the 2 df together
# update = update.merge(dfPO2, how='outer', right_index=False).drop_duplicates(subset=["PO#", "Catalog#"], keep="first")
# update_clean = update.groupby(["Requisition#", "Catalog#"]).first().reset_index()
# update2 = update_clean[col_orders]
# print(len(update2.index), "Df final")
#
# ##Generating a list of any missing Req#
# diff = pd.DataFrame(list(set(update2["Requisition#"]).symmetric_difference(test["Requisition#"])))
#
# ##Adding Date Added Values
# today = date.today()
# update2.loc[:,"Date Added"] = pd.to_datetime(update2["Date Added"])
# update2.loc[update2["Date Added"].isnull(),"Date Added"]=today
#
# ##change Date format from dd-mm-yyyy to %Y-%m-%d
# update2.loc[:,"Creation Date"] = pd.to_datetime(update2["Creation Date"], dayfirst=True)
# update2.loc[:,"Submission Date"] = pd.to_datetime(update2["Submission Date"], dayfirst=True)
# update2.loc[:,"PO date"] = pd.to_datetime(update2["PO date"], dayfirst=True)
#
# ##adjust missing Cat#
# update2.loc[update2["Catalog#"].isnull(),"Catalog#"]="None"
# update2.sort_values(by=["PO date", "Requisition#", "Line#"], ascending=False, inplace=True)
#
# # print(update2.dtypes)
# ###4 Re-export UpdatedOrders
# update2.to_csv("UpdatedOrders.csv", index=False)

# update2 = pd.read_csv("UpdatedOrders.csv")
update2 = pd.read_csv(glob("OrdersFromR*.csv")[0])
diff = pd.read_csv(glob("MissingReq*.csv")[0])

update2.sort_values(by=["POdate", "RequisitionNo", "LineNo"], ascending=False, inplace=True)
# print(update2.dtypes, "post csv")
###5 Setting up reports
##a) Missing values
missing = update2.loc[update2.isnull().any(axis=1)&(update2["PO_No"].notnull())&(~update2['Requisitioner'].str.contains("Timoth", na=False))]
missing.to_csv("Missing_Info.csv", index=False)

##b) Requisition w/o PO#; Flag if date is older than 10 days
noPO_col = ['DateAdded', 'RequisitionNo', 'CreationDate',
       'SubmissionDate', 'POdate', 'Requisitioner',
       'VendorName', 'VendorID', 'ProductDescription', 'CatalogNo', 'Qty', 'LineStatus']
noPO = update2.loc[update2["PO_No"].isnull()&(~update2['LineStatus'].str.contains("Rejected", na=False))&
                        (~update2['Requisitioner'].str.contains("Timoth", na=False))]
noPO = noPO[noPO_col]
noPO.to_csv("Missing_POs.csv", index=False)

##c) PO sent to vendor, Order not received; Flag if date is older than 30 days
Rcpt_col = ['DateAdded', "Requisitioner", 'RequisitionNo', 'PO_No', 'LineNo', 'CreationDate',
       'SubmissionDate', 'POdate', 'VendorName','ProductDescription', 'CatalogNo', 'Qty', 'ReceivedQty',
       'InvoicedQty', 'LineStatus', 'ReceiveStatus', 'InvoiceStatus','SettlementStatus']
notReceived = update2.loc[(update2["PO_No"].notnull())&(update2["ReceivedQty"]==0)&(~update2["ReceiveStatus"].str.contains("Fully", na=False))&
            (~update2['LineStatus'].str.contains("Rejected", na=False))&(~update2['VendorName'].str.contains("Genome Centre", na=False))&
            (~update2["InvoiceStatus"].str.contains("Fully Invoiced", na=False))&(~update2['VendorName'].str.contains("Genome Centre", na=False))]
notReceived = notReceived[Rcpt_col]
notReceived.to_csv("Orders_to_receive.csv", index=False)

##d) Partial Reception
partReceive = update2.loc[(update2["PO_No"].notnull())&(update2["ReceivedQty"]>0)&(update2["ReceivedQty"] < update2["Qty"])&
            (~update2["InvoiceStatus"].str.contains("Fully Invoiced", na=False))&(~update2["SettlementStatus"].str.contains("Over-Invoiced", na=False))&
            (~update2["SettlementStatus"].str.contains("Fully Matched", na=False))&(~update2['VendorName'].str.contains("Genome Centre", na=False))]
partReceive = partReceive[Rcpt_col]
partReceive.to_csv("Partial_Receptions.csv", index=False)

#e) Invoice qty > Reception qty
col_to_keep = ['DateAdded', "Requisitioner", 'RequisitionNo', 'PO_No', 'POdate', 'VendorName', 'ProductDescription', 'CatalogNo', 'Qty', 'ReceivedQty',
       'InvoicedQty', 'SettlementStatus']
InoR =  update2.loc[(update2["ReceivedQty"] < update2["InvoicedQty"])&(~update2["InvoiceStatus"].str.contains("Fully Invoiced", na=False))&
                    (~update2["SettlementStatus"].str.contains("Fully Matched", na=False))]
InoR = InoR[col_to_keep]
InoR.to_csv("Discrepancy_Invoice-Reception.csv", index=False)


### Exporting All df to an excel file (Archive the old file about once every 2 months)
def format_sheet(df, sheet_name):
    worksheet = writer.sheets[sheet_name]
    worksheet.set_zoom(80)
    for column in df:
        column_width = max(df[column].astype(str).map(len).max(), len(column))
        if column_width <= 5:
            column_width = 5
        col_idx = df.columns.get_loc(column)
        worksheet.set_column(col_idx, col_idx, column_width)
    col_idx = df.columns.get_loc('ProductDescription')
    worksheet.set_column(col_idx, col_idx, 35)
    col_idx = df.columns.get_loc('DateAdded')
    worksheet.set_column(col_idx, col_idx, 14)



df_sheets = {'Current Orders': update2, 'Missing Informations':missing,
            'Requisition without PO':noPO, 'Orders Not Received':notReceived,
            'Orders Partially Received':partReceive, 'Invoice without Reception':InoR}

filename = 'OrdersTracking_'+datetime.now().strftime("%Y-%m-%d")+".xlsx"
print(filename, " Created")

writer= pd.ExcelWriter(filename, datetime_format='yyyy-mm-dd', engine='xlsxwriter')
diff.to_excel(writer, sheet_name='Missing Req.', index=False)
# update.to_excel(writer, sheet_name='Current Orders no PO update', index=False)
update2.to_excel(writer, sheet_name='Current Orders', index=False)
missing.to_excel(writer, sheet_name='Missing Informations', index=False)
noPO.to_excel(writer, sheet_name='Requisition without PO', index=False)
notReceived.to_excel(writer, sheet_name='Orders Not Received', index=False)
partReceive.to_excel(writer, sheet_name='Orders Partially Received', index=False)
InoR.to_excel(writer, sheet_name='Invoice without Reception', index=False)

workbook  = writer.book
# Add a format. Light red fill with dark red text.
format1 = workbook.add_format({'bg_color': '#FFC7CE',
                           'font_color': '#9C0006'})

# Add a format. Yellow fill with dark yellow text.
format2 = workbook.add_format({'bg_color': '#FFEB9C',
                           'font_color': '#9C6500'})

for sheet_name, df in df_sheets.items():
    format_sheet(df, sheet_name)

worksheet = writer.sheets['Current Orders']
col_idx = update2.columns.get_loc("Fund")
worksheet.set_column(col_idx, col_idx, 10)
col_idx = update2.columns.get_loc("Activity")
worksheet.set_column(col_idx, col_idx, 10)

worksheet = writer.sheets['Missing Informations']
col_idx = missing.columns.get_loc("Fund")
worksheet.set_column(col_idx, col_idx, 10)
col_idx = missing.columns.get_loc("Activity")
worksheet.set_column(col_idx, col_idx, 10)


worksheet = writer.sheets['Requisition without PO']
worksheet.set_zoom(80)
worksheet.conditional_format("$A$1:$L$%d" % (len(noPO.index)+1),
                         {"type": "formula",
                          "criteria": '=AND((TODAY()-$D1)>=10, (TODAY()-$D1)<366)', "format": format1})
worksheet = writer.sheets['Orders Not Received']
worksheet.set_zoom(80)
worksheet.conditional_format("$A$1:$R$%d" % (len(notReceived.index)+1),
                         {"type": "formula",
                          "criteria": '=AND((TODAY()-$G1)>25, (TODAY()-$G1)<90)', "format": format2})
worksheet.conditional_format("$A$1:$R$%d" % (len(notReceived.index)+1),
                         {"type": "formula",
                          "criteria": '=AND((TODAY()-$G1)>=90, (TODAY()-$G1)<366)', "format": format1})

writer.save()
print("End of script")
