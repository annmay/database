#script to coordinate the MMP update

#moving old files into Archive
mkdir -p Archive

mv ./OrdersTracking*.xlsx  ./Archive/
if [ -f CurrentOrders*.csv ]
then
  mv ./OrdersFromR*.csv  ./Archive/
else
  mv ./OrdersFromR*.csv  ./CurrentOrders9.csv
fi

TZ="America/New_York" Rscript Update_MMP.r
python3 UpdateMMP.py
#Rscript finaceReport.r

mv ./CurrentOrders*.csv  ./Archive/
mv ./RequisitionData*.csv ./Archive/
mv ./POData*.csv ./Archive/
mv ./MissingReq*.csv ./Archive/
