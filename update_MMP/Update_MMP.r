#Script for updating MMP orders Tracking and producing trimestrial reports 

#Libraryr import 
if (!require("tidyverse")) install.packages("tidyverse", dep = TRUE); library("tidyverse")

#============Loading the starting data=================
filepath <- "/mnt/c/Users/aroy/Documents/database/update_MMP/"

CurrentOrders <- read.csv(dir(filepath, full.names=T, pattern="^CurrentOrders")[1])
#Loading Requisition data
ReqData <- read.csv(dir(filepath, full.names=T, pattern="^RequisitionData")[1])[1:19]
#Loading PO data 
POData <- read.csv(dir(filepath, full.names=T, pattern="^POData")[1])[1:21]

#Adjusting Column names
#Extract colname from Current 
colname <- names(CurrentOrders)
names(ReqData) = c(colname[2], "PR_Name", colname[6], colname[5], "CompleteDate", colname[8], 
                   colname[4], colname[11:12], colname[9:10], colname[13:14], colname[16], 
                   colname[15], colname[17:18], "LineStatus", "RequisitionTotal")

names(POData) = c("PO_ID", colname[3], "Status", colname[2],  colname[8], colname[7], colname[11], 
                  colname[9:10], colname[4], "PO_LineID", colname[13:17], colname[22], colname[19], 
                  colname[23], colname[20], colname[24])

#Tweak format 
ReqData$Fund <- ReqData$Fund %>% as.character()
ReqData$SubmissionDate <- ReqData$SubmissionDate %>% as.Date.character(tryFormats = c("%d/%m/%Y"))
ReqData$CreationDate <- ReqData$CreationDate %>% as.Date.character(tryFormats = c("%d/%m/%Y"))
ReqData$CompleteDate <- ReqData$CompleteDate %>% as.Date.character(tryFormats = c("%d/%m/%Y"))
CurrentOrders$CreationDate <- as.Date(CurrentOrders$CreationDate) 
CurrentOrders$DateAdded <- as.Date(CurrentOrders$DateAdded)
CurrentOrders$SubmissionDate <- as.Date(CurrentOrders$SubmissionDate)
CurrentOrders$POdate <- as.Date(CurrentOrders$POdate)

POData$POdate <- POData$POdate %>% as.Date.character(tryFormats = c("%d/%m/%Y"))

#Change PO status of none to NA
# head(POData$ReceiveStatus)
# POData$ReceiveStatus <- na_if(POData$ReceiveStatus, "none")
# POData$InvoiceStatus <- na_if(POData$InvoiceStatus, "none")


#160 rows of NA in PO# and PO date
CurrentOrders <- CurrentOrders %>% distinct() %>% drop_na() 


#=======UPDATING REQUISITIONS==============
common <- intersect(names(ReqData), names(POData))[c(1, 3, 8)]

# ReqUpdate <- CurrentOrders %>% full_join(ReqData, by=intersect(names(ReqData), colname)[c(1:3,5:9, 11:15)]) %>%
#   mutate(`Requisitioner` = coalesce(`Requisitioner.x`, `Requisitioner.y`)) %>% 
#   mutate(`Product Description` = coalesce(`Product Description.x`, `Product Description.y`)) %>% 
#   select(colname) #1915 obs. (test =1780)
Reqcommon <- intersect(names(ReqData), colname)
ReqUpdate <- CurrentOrders %>% rows_upsert(ReqData[Reqcommon], by=common)#CurrentOrders = 2223 obs, ReqUpdate = 1860obs

ReqUpdate$DateAdded <- ReqUpdate$DateAdded %>% replace_na(Sys.Date()) 

ReqUpdateNA <- ReqUpdate %>% filter_all(any_vars(is.na(.)))  #193 observations (test = 4)

POcommon <- intersect(names(POData), colname)
PO_update <- ReqUpdate %>% rows_upsert(POData[POcommon], by=common)#CurrentOrders = 2223 obs, ReqUpdate = 1860obs
PO_update$DateAdded <- PO_update$DateAdded %>% replace_na(Sys.Date()) 

PO_updateNA <- PO_update %>% filter_all(any_vars(is.na(.))) #CurrentOrders = 636 obs, ReqUpdate = 273 obs 


write.csv(PO_update, paste(filepath, "OrdersFromR_", Sys.Date(), ".csv", sep=""), row.names = FALSE)

#tests==========

dbl3 <- ReqUpdate %>% group_by(RequisitionNo, LineNo) %>% filter(n()>1)# %>% summarize(n=n())
dbl6 <- PO_update %>% group_by(RequisitionNo, LineNo) %>% filter(n()>1)# %>% summarize(n=n())

subtest <- PO_update %>% subset(is.na(CatalogNo)) %>% subset(is.na(`Qty`))
subset <- PO_update %>% subset(PO_update$PO_No  == "P0952454" )

missingReq <- setdiff(POData$RequisitionNo, ReqData$RequisitionNo) %>% setdiff(CurrentOrders$RequisitionNo)
write.csv(missingReq, paste(filepath,"MissingReq_", Sys.Date(), ".csv", sep=""), row.names = FALSE)
# test5 <- mutate(net4, ave = ifelse(ave < 10 & ave != temp2, temps2 / NNET * NET, ave))
